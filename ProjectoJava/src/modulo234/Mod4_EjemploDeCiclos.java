package modulo234;

import java.util.Scanner;

public class Mod4_EjemploDeCiclos {

	public static void main(String[] args) {
		//ejemplo de un ciclo for
		//realizar la tabla de multiplicar que se ingrese
		System.out.println("ingrese una tabla de mostrar");
		//ingresar la tabla
		Scanner scan = new Scanner(System.in);
		int tabla = scan.nextInt();
		
		//cuando tengo una cantidad fija
		//inicio ; condicion de corte; incremente
		//i = i+1
		System.out.println("tabla del " + tabla);
		System.out.println("\n\n");
		for(int i=0     ;i<10;i++){
			int result = tabla * i;
			System.out.println(tabla + "*" + i + "=" + result);
		}
		
		//la estructura del while
		//por ejemplo quiero cortar el cone el primer numero impar
		System.out.println("el ciclo cortara con el primer impar que se ingrese");
		
		System.out.println("ingrese un numero");
		int numero = scan.nextInt();
		int resto = numero %2;
		while(resto==0){
			System.out.println("ingrese un numero");
			numero = scan.nextInt();
			resto = numero %2;			
		}
		
		
	}

}
