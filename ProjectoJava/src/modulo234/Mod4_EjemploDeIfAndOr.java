package modulo234;

import java.util.Scanner;

public class Mod4_EjemploDeIfAndOr {

	public static void main(String[] args) {
		//debo ingresar 3 numeros y determinar cual es el mayor
		System.out.println("Ingrese un numero");
		Scanner scan = new Scanner(System.in);
		int a = scan.nextInt();
		
		System.out.println("Ingrese un numero");
		int b = scan.nextInt();
		
		System.out.println("Ingrese un numero");
		int c = scan.nextInt();
       
		if(a>b && a>c)
			System.out.println("el mayor es " + a);
		else if (b>a && b >c)
			System.out.println("el mayor es " + b);
		else if(c>a && c>b)
			System.out.println("el mayor es " + c);
		else 
			System.out.println("son todos iguales");
	}

}
