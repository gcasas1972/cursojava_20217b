package modulo234;

import javax.swing.plaf.synth.SynthSeparatorUI;

public class EjemploVarialbes {

	public static void main(String[] args) {
		//enteros
		byte b = 127;
		int i = 34000;
		long l = 23424;

		long suma = b+i+l;
		float division = (float)i/b;
		//de prepo
		byte otroByte = (byte)i;
		
		char letra = 'a';
		int resto = b%2;
		
		System.out.println("b="+ b);
		System.out.println("i=" + i);
		System.out.println("l=" + l);
		System.out.println("otroByte=" + otroByte);
		System.out.println("resto=de " + b + " divididor por 2 da "  + resto);
		
		System.out.println("suma="+ suma);
		System.out.println("division=" + division);
		
		
		

	}

}
