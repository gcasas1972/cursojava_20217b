package modulo234;

import java.util.Scanner;

public class Mod4_ejemploDeAcumlador {
	public static void main(String[] args) {
		
		//realizar un programa que permita ingresar valores y de ellos
		//el ingreso se finalizar'a con el -99
		//saber 1-la suma de todos
		//2- el promedio
		//3- el maximo 
		//4- el minimo
		//5-  contar lo pares
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("ingrese un valor numerico");		
		float valor =scan.nextFloat();
		
		float maximo = valor;
		float minimo = valor;
		
		//voy a guardar toda la suma
		float suma=0;
		int cont =0; //voy a contador
		int contPares=0;
		
		
		while (valor!=-99){
			if(valor>maximo)
				maximo=valor;
			
			if(valor < minimo)
				minimo=valor;
			if(valor%2==0)
				contPares++;
		
			suma = suma + valor; //acumulador
			cont++;
			System.out.println("ingrese un valor numerico");		
			valor =scan.nextFloat();			
		}
		
		System.out.println("la suma es " + suma);
		System.out.println("la cantidad ingresada es " + cont);
		System.out.println("el promedio es " + (suma/cont));
		System.out.println("el maximmo es " + maximo);
		System.out.println("el minimo es " + minimo);
		System.out.println("pares " + contPares);
	}

}
