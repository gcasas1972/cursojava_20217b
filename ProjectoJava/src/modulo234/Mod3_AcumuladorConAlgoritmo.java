package modulo234;

public class Mod3_AcumuladorConAlgoritmo {

	public static void main(String[] args) {
		//quiero sumar los pares
		//random me un numero entre 0 y 1 por ej 0.2344633420439852598234643838902809348208059
		int sumaMagica=0;
		for(int i =0;i<3;i++){
			int numero = (int)(Math.random()*100);			
			int numeroMagico = (numero%2-1)*(-1);
			//13 , resto 1 -1 = 0 * (-1) = 0
			//6  , resto 0 -1 = -1 * (-1) = 1
			sumaMagica = sumaMagica + numero * numeroMagico;
			System.out.println("numero=" + numero );			
		}
		
		System.out.println("\n\nsuma = " + sumaMagica);
		
		
		

	}

}
