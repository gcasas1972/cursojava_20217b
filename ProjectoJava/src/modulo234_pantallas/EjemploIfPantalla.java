package modulo234_pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

public class EjemploIfPantalla {
	//estos son los fields definidos
	//1-definicion
	private JFrame frame;
	private JTextField textNota1;
	private JTextField textNota2;
	private JTextField textNota3;
	private JLabel lblResult;
	private JLabel lblImagen;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EjemploIfPantalla window = new EjemploIfPantalla();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public EjemploIfPantalla() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setFont(new Font("Tahoma", Font.BOLD, 16));
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblCalculoDePromedio = new JLabel("calculo de promedio");
		lblCalculoDePromedio.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 24));
		lblCalculoDePromedio.setBounds(111, 27, 252, 29);
		frame.getContentPane().add(lblCalculoDePromedio);
		
		JLabel lblNota = new JLabel("Nota 1:");
		lblNota.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblNota.setBounds(36, 95, 60, 20);
		frame.getContentPane().add(lblNota);
		
		JLabel lblNota_2 = new JLabel("Nota 2:");
		lblNota_2.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblNota_2.setBounds(36, 126, 60, 20);
		frame.getContentPane().add(lblNota_2);
		
		JLabel lblNota_1 = new JLabel("Nota 3:");
		lblNota_1.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblNota_1.setBounds(36, 159, 60, 20);
		frame.getContentPane().add(lblNota_1);
		//2- se crea con la sentencia new
		textNota1 = new JTextField();
		//3- se asignan valore o se usan sus servicios
		textNota1.setFont(new Font("Tahoma", Font.BOLD, 16));
		textNota1.setBounds(113, 97, 86, 20);
		//en el caso particular de los objetos graficos dedben meterse adentro de un contenedor
		frame.getContentPane().add(textNota1);
		textNota1.setColumns(10);
		
		textNota2 = new JTextField();
		textNota2.setFont(new Font("Tahoma", Font.BOLD, 16));
		textNota2.setColumns(10);
		textNota2.setBounds(111, 128, 86, 20);
		frame.getContentPane().add(textNota2);
		
		textNota3 = new JTextField();
		textNota3.setFont(new Font("Tahoma", Font.BOLD, 16));
		textNota3.setColumns(10);
		textNota3.setBounds(111, 161, 86, 20);
		frame.getContentPane().add(textNota3);
		
		JButton btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//1- cuando apretes el boton hace estooooo
				String strNota1 = textNota1.getText();
				String strNota2 = textNota2.getText();
				String strNota3 = textNota3.getText();
				
				//2- convertir el string en un float
				//wrapper float Float, int Ingeter , char Character, 
				float nota1 = Float.parseFloat(strNota1);
				float nota2 = Float.parseFloat(strNota2);
				float nota3 = Float.parseFloat(strNota3);
				
				float promedio = (nota1+nota2+nota3)/3;
				if(promedio>=7){
					lblResult.setBackground(Color.GREEN);
					lblImagen.setIcon(new ImageIcon(EjemploIfPantalla.class.getResource("/iconos/Gano_32px.png")));
				}
					
				else{
					lblResult.setBackground(Color.RED);
					lblImagen.setIcon(new ImageIcon(EjemploIfPantalla.class.getResource("/iconos/fantasma_32px.png")));
				}
					
				
				// convertir el float a un String
				String strResult = Float.toString(promedio);
				
				lblResult.setText(strResult);
				
				//borramos todo para que quede limpito
				textNota1.setText("");
				textNota2.setText("");
				textNota3.setText("");
				
			}
		});
		btnCalcular.setFont(new Font("Tahoma", Font.BOLD, 16));
		btnCalcular.setBounds(74, 203, 101, 29);
		frame.getContentPane().add(btnCalcular);
		
		JLabel lblPromedio = new JLabel("promedio");
		lblPromedio.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblPromedio.setBounds(229, 100, 77, 20);
		frame.getContentPane().add(lblPromedio);
		
		lblResult = new JLabel("");
		lblResult.setBackground(Color.ORANGE);
		lblResult.setOpaque(true);
		lblResult.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblResult.setBounds(229, 131, 77, 20);
		frame.getContentPane().add(lblResult);
		
		lblImagen = new JLabel("");		
		lblImagen.setBounds(256, 169, 52, 47);
		frame.getContentPane().add(lblImagen);
	}
}
