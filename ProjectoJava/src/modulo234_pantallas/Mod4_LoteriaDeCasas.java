package modulo234_pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.DefaultComboBoxModel;
import javax.swing.AbstractListModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;

public class Mod4_LoteriaDeCasas {

	private JFrame frame;
	private JComboBox cmbNumero;
	private JButton btnJugar;
	private JList lstNumeros;
	private String strNumeros[] = new String[100];
	private String[] values = new String[100];
	private JScrollPane scrollPane;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Mod4_LoteriaDeCasas window = new Mod4_LoteriaDeCasas();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Mod4_LoteriaDeCasas() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 549, 440);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("LOTERIA DE CASAS");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 30));
		lblNewLabel.setBounds(114, 23, 293, 37);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblElijaUnNumero = new JLabel("Elija un numero");
		lblElijaUnNumero.setFont(new Font("Tahoma", Font.BOLD, 24));
		lblElijaUnNumero.setBounds(31, 118, 188, 29);
		frame.getContentPane().add(lblElijaUnNumero);
		
		btnJugar = new JButton("");
		btnJugar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				for(int i = 0 ; i<100;i++){
					int numeroAleatorio= (int)(Math.random()*100);
					values[i]=Integer.toString(numeroAleatorio);
				}
					
				
				//esto lo hizo el plugin
				lstNumeros.setModel(new AbstractListModel() {			
					public int getSize() {
						return values.length;
					}
					public Object getElementAt(int index) {
						return values[index];
					}
				});
				
			}
		});
		btnJugar.setToolTipText("Jugar");
		btnJugar.setIcon(new ImageIcon(Mod4_LoteriaDeCasas.class.getResource("/iconos/rayo_32px.png")));
		btnJugar.setBounds(31, 256, 67, 41);
		frame.getContentPane().add(btnJugar);
		
		cmbNumero = new JComboBox();
		//huuuuuu voy a cargar mi combo
		for(int i=0;i<100;i++)
			strNumeros[i] = Integer.toString(i);
		
		
		cmbNumero.setModel(new DefaultComboBoxModel(strNumeros));
		cmbNumero.setBounds(254, 124, 102, 29);
		cmbNumero.setFont(new Font("Tahoma", Font.ITALIC | Font.ITALIC, 14));
		frame.getContentPane().add(cmbNumero);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(254, 164, 102, 227);
		frame.getContentPane().add(scrollPane);
		
		lstNumeros = new JList();
		scrollPane.setViewportView(lstNumeros);
	}
}
